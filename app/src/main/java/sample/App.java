package sample;

import org.glycoinfo.subsumption.manipulation.LevelDeterminator;
import org.glycoinfo.subsumption.manipulation.Level;
import org.glycoinfo.subsumption.manipulation.GraphManager;

public class App {

    public static void main(String[] args) {
      String w = "WURCS=2.0/3,5,4/[a2122h-1b_1-5_2*NCC/3=O][a1122h-1b_1-5][a1122h-1a_1-5]/1-1-2-3-3/a4-b1_b4-c1_c3-d1_c6-e1";

      LevelDeterminator level_determinator = new LevelDeterminator();
      Level l = level_determinator.getSubsumptionLevel(GraphManager.toGraph(w));
      System.out.println(l.getLevel());
      System.out.println(l.getClassName());
    }
}
